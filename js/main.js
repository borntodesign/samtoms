$show_contents = $(".js-show-contents");
$contents = $(".contents")
$body = $("body")
$contents_close = $(".contents__close")
$contents_list = $(".contents__listItem")

$show_contents .click(function(){
  $contents.addClass("is-active");
  $body.toggleClass("menu-open");
  $contents.css({
    'opacity' : 1,
    'transform' : 'scale(1)',
    'visibility' : 'visible',
    'display' : 'block'
  });
});
$contents_close.click(function(){
  $contents.removeClass("is-active");
  $body.toggleClass("menu-open");
  $contents.css({
    'opacity' : 0,
    'transform' : 'scale(0.8)'
  });
  setTimeout(function(){
    $contents.css({
      'visibility' : 'hidden'
    });
  },300);
});
$contents_list.click(function(){
  $contents.removeClass("is-active");
  $body.toggleClass("menu-open");
  $contents.css({
    'opacity' : 0,
    'transform' : 'scale(0.8)'
  });
  setTimeout(function(){
    $contents.css({
      'visibility' : 'hidden'
    });
  },300);
});
